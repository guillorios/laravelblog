<!-- Modal -->
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <form method="POST" action="{{ route('admin.posts.store') }}">
        @csrf
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Agrega el título de la publicación</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }} ">
                    {{-- <label for="">Título de la Publicación</label> --}}
                    <input name="title" class="form-control" type="text" placeholder="Ingresa un título a la publicación" required value = "{{ old('title') }}">
                    {!! $errors->first('title', '<span class="help-block">:message</span>') !!}
                </div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            <button class="btn btn-primary">Crear publicación</button>
            </div>
        </div>
        </div>
    </form>
</div>
