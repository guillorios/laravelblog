<?php

use Faker\Generator as Faker;

$factory->define(App\Tag::class, function (Faker $faker) {
    $title = $faker->sentence(2);
    return [
        'name' => $title,
    ];
});
