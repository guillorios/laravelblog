<?php

use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(App\Post::class, function (Faker $faker) {
    $title = $faker->sentence(4);
    $startDate = Carbon::createFromTimeStamp($faker->dateTimeBetween('-1 years', '+1 month')->getTimestamp());

    return [
        'title' => $title,
        'excerpt' => $faker->text(10),
        'body' => $faker->text(500),
        'category_id' => rand(1,20),
        'published_at' =>  $startDate,
    ];
});
